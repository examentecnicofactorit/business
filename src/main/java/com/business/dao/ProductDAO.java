
package com.business.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.business.models.Product;

public interface ProductDAO extends JpaRepository<Product, Long> {
	
}

package com.business.dao;

import java.util.Date;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.business.models.SpecialDate;
import com.business.models.User;

public interface SpecialDateDAO extends JpaRepository<SpecialDate, Date> {

	
	@Query("SELECT prom_date FROM special_date_tbl u WHERE prom_date = CURRENT_DATE")
	Date getSpecialDate();

}

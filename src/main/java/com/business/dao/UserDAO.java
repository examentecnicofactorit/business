package com.business.dao;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.business.models.User;

public interface UserDAO extends JpaRepository<User, String>{
	
	
	@Query("SELECT u.user_email, u.user_pass, u.user_name, u.user_type, u.user_phone FROM user_tbl u WHERE u.user_email = ?1 AND u.user_pass = ?2")
	User getUser(String email, String pass);
	
	


}

package com.business;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Business2Application {

	public static void main(String[] args) {
		SpringApplication.run(Business2Application.class, args);
	}

}

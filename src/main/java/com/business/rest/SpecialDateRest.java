package com.business.rest;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.business.dao.SpecialDateDAO;
import com.business.models.Product;

import org.springframework.web.bind.annotation.RequestMethod;

@RestController
@RequestMapping("v1/dates")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET})
public class SpecialDateRest {
	
	@Autowired
	private SpecialDateDAO specialDateDAO;
	
	
	@GetMapping("/getSpecialDate")
	public Date getProducts() {
		return specialDateDAO.getSpecialDate();
	}

}

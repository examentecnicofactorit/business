package com.business.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpSession;
import javax.ws.rs.BeanParam;




import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.business.dao.UserDAO;
import com.business.models.User;

@RestController
@RequestMapping("v1/user")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST, RequestMethod.PUT})
public class UserRest {
	@Autowired
	private UserDAO userDAO;
	
		
	@PostMapping("/insert")
	public void insertDB(@RequestBody User user) {
		userDAO.save(user);
	}
	
	
	@GetMapping("/getUsers")
	public List<User> getUsers() {
		return userDAO.findAll();
	}
	
	@PutMapping("/update")
	public void updateUser(@RequestBody User user) {
		userDAO.save(user);
	}
	
	@PostMapping("/login")
	public User login(@RequestBody User user) {
		List<User> lista = userDAO.findAll();
		for(User usuario : lista) {
			if(usuario.getEmail().equals(user.getEmail()) && usuario.getPass().equals(user.getPass()) ) {
				return usuario; 
			}
		}
		return null;
	}
	
}

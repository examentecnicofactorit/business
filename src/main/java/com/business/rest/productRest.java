package com.business.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMethod;

import com.business.dao.ProductDAO;
import com.business.models.Product;

@RestController
@RequestMapping("v1/products")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET})
public class productRest {

	@Autowired
	private ProductDAO productDao;
	
	
	@GetMapping("/getProducts")
	public List<Product> getProducts() {
		return productDao.findAll();
	}
}

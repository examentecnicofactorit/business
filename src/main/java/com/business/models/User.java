package com.business.models;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.sun.istack.NotNull;

@Entity(name ="user_tbl")
@Table(name = "user_tbl")
public class User implements Serializable {
	
		@Id
		@Column(name = "user_email")
		private String user_email;
		
		@NotNull
		@Column(name = "user_pass", nullable = false)
		private String user_pass;
		
		@NotNull
		@Column(name = "user_type")
		private String user_type;

		@NotNull
		@Column(name = "user_name", nullable = false)
		private String user_name;
		
		@Column(name = "user_phone")
		private String user_phone;
		
		public User() {}
		public User(String name, Long id) {
			this.user_name = name;
		}

		public String getType() {
			return user_type;
		}


		public String getName() {
			return user_name;
		}

		public void setName(String name) {
			this.user_name = name;
		}

		
		public String getEmail() {
			return user_email;
		}

		public void setEmail(String email) {
			this.user_email = email;
		}

		public String getPass() {
			return user_pass;
		}

		public void setPass(String pass) {
			this.user_pass = pass;
		}

		public void setType(String type) {
			this.user_type = type;
		}

		
		
	
}

package com.business.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "product_tbl")
public class Product implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private  Long  id;
	@Column(name = "product_value", nullable = false, scale = 2)
	private Double product_value;
	@Column(name = "product_name", nullable = false)
	private String name;
	
	public Product() {}
	public Product(Long id, Double value, String name ) {
		this.id = id;
		this.product_value = value;
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getValue() {
		return product_value;
	}

	public void setValue(Double value) {
		this.product_value = value;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}

package com.business.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name = "special_date_tbl")
@Table(name = "special_date_tbl")
public class SpecialDate {
	
	@Id
	@Column(name = "prom_date")
	private Date prom_date;

	public Date getProm_date() {
		return prom_date;
	}

	public void setProm_date(Date prom_date) {
		this.prom_date = prom_date;
	}
	

}
